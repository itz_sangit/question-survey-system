## Run
Copy .env.example to .env and setup your Database information.

``` bash

# Composer install
composer install

# init
php artisan migrate
php artisan db:seed
# or
./mrs.sh

# serve at localhost:8000
php artisan serve

```

 Use DB seed for seeding user and questions.
# Login (if you use DB seed)
    user admin@sangit.info
    pass: secret