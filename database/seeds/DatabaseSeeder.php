<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersTableSeeder::class);
        $questions = [
            [
                'text' =>'Where is Innoscripta located?',
                'options_json' => '["Germany","Sweden","England","Japan"]'
            ],
            [
                'text' => 'What is our average time to concretize a project?',
                'options_json' => '["Within a Month","Earliest time possible","Less than a Month","More than a Month"]'
            ],
            [
                'text' => 'How many medium-sized partners nationwide are we connected with?',
                'options_json' => '["More than 1000","More than 10000","More than 200","More than 2000"]'
            ],
            [
                'text' => 'Innoscripta has an overall success rate of?',
                'options_json' => '["10%","65%","90%","100%"]'
            ],
        ]; 

        foreach ($questions as $key => $value) {
            DB::table('questions')->insert([
                'text' => $value['text'],
                'options_json' => $value['options_json'],
            ]);
        }
    }
}
