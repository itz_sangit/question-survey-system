@extends('layouts.app') 
@section('content')
<div class="container">
    <div class="row justify-content-center" style="">
        <div class="col-md-10 inno-box">
            <button class="btn btn-block inno-btn" onclick="add_question()" style="margin:2%" data-toggle="modal" data-target="#question_modal"> Add New Question</button>
            <div class="table-responsive">
                <table class="table table-hover table-light" id="question_table">
                    <tr>
                        <th>No</th>
                        <th>Question</th>
                        <th>Options</th>
                        <th>Action</th>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</div>
</div>

<!-- The Modal -->
<div class="modal" id="question_modal">
    <div class="modal-dialog">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">Add new Question</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <!-- Modal body -->
            <div class="modal-body">
                <form id="questionForm" class="form-horizontal">
                    <div class="alert alert-info">
                        <div class="form-group row">
                            <label for="name" class="col-sm-12">Question Text</label>
                            <div class="col-sm-12">
                                <textarea class="form-control" autocomplete="on" required id="text" placeholder="Question"></textarea>
                            </div>
                            <div class="help-block with-errors"></div>
                        </div>
                    </div>
                    <div class="alert alert-info">
                        <div class="form-group row">
                            <label for="name" class="col-sm-12">Options (Type Option and Press Enter)</label>
                            <div class="col-sm-12">
                                <select class="form-control select2" id="options_json" multiple="multiple" style="width:100%;" required>
                                            
                                    </select>
                            </div>
                            <div class="help-block with-errors"></div>
                        </div>
                    </div>
                </form>
            </div>

            <!-- Modal footer -->
            <div class="modal-footer">
                <span id="modal_btn"><button type="button" class="btn btn-success" onclick="save_new_data()">Add New Question</button></span>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
            </div>

        </div>
    </div>
</div>
{{-- external script for this page --}}
    @include('question.scripts')
@endsection