<script>
    $(function(){
        $(".select2").select2({
            tags: true,
            tokenSeparators: [',', ' ']
        })
        $("#questionForm").validator();

        $('#question_table').DataTable({
            processing: true,
            serverSide: true,
            "bInfo" : false,
            ajax: 'questions',
            columns: [
                { "title": "No",data: 'id', name: 'id'},
                { "title": "Question",data: 'text', name: 'text'},
                { "title": "Options ",data: 'options', name: 'options'},
                { "title": "Action",data: 'action', name: 'text'},
            ]
        });
    });

    function edit_question(id) {
        $(".modal-title").text("Edit Question " + id);
        $("#modal_btn").html(`<button type="button" class="btn btn-success" onclick="save_new_data_edit(${id})">Update Question</button>`);
        $("#options_json").val(null).trigger('change');
        $("#text").val("");

        EasyAjax.get("questions/"+id).done(function(data){
            $("#text").val(data.text)
    
            data.options.forEach(element => {
                var newState = new Option(element, element, true, true);
                $("#options_json").append(newState);
            });
          
            $("#options_json").trigger('change');
        });
    }
    function add_question() {
        $(".modal-title").text("Add Question");
        $("#modal_btn").html(`<button type="button" class="btn btn-success" onclick="save_new_data()">Add New Question</button>`);
        $("#options_json").val(null).trigger('change');
        $("#text").val("");

    }
    function save_new_data_edit(id) {
        
        if (isFormValid('questionForm')) {
            let data  = getFormData('questionForm');
            if(data.options_json.length == 0)
            {
                Swal.fire(
                'Problem',
                'No options Provided',
                'error'
                );
                return;
            }
            EasyAjax.put(data,'questions/'+id).done(function(){
                reloadTable();
                $("#question_modal").modal('hide');

            });        
        }

    }
    function save_new_data() {
        if (isFormValid('questionForm')) {
            let data  = getFormData('questionForm');
            if(data.options_json.length == 0)
            {
                Swal.fire(
                'Problem',
                'No options Provided',
                'error'
                );
                return;
            }
            EasyAjax.post(data,'questions').done(function(){
                reloadTable();
                $("#question_modal").modal('hide');
            });        
        }

    }

    function reloadTable() {
        $('#question_table').DataTable().ajax.url('questions').load();

    }
    function deleteQuestion(id) {
        EasyAjax.delete('questions/'+id).done(function(){
            $("#question_modal").modal('hide');
            reloadTable();

        }); 
    }

</script>