<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Innoscripa')}} Question Survey</title>
    @include('layouts.external-includes')

</head>
    @include('layouts.style')

<body>
    <div id="app">
        <nav class="navbar navbar-expand-md navbar-light navbar-laravel" style="background:white;">
            <div class="container">
                <a class="navbar-brand" href="{{ url('/') }}" style="color:white;!important">
                <img src="{{URL::to('logo.png')}}" alt="" width="150px">
                    {{ config('app.name', 'Innoscripa')}} 
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                    aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">
                    </ul>
                    @guest
                    <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a> @else
                    <a href="{{URL::to('/')}}" class="nav-link btn inno-btn-li {{Request::is('/') ? 'inno-btn-li-active' : '' }}">Survey</a>
                    <a href="{{URL::to('home')}}" class="nav-link btn inno-btn-li {{Request::is('home') ? 'inno-btn-li-active' : '' }}">Home</a>
                    <a href="{{URL::to('questions')}}" class="nav-link btn inno-btn-li {{Request::is('questions') ? 'inno-btn-li-active' : '' }}">Manage Questions</a>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">

                        <li class="nav-item dropdown">
                            <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true"
                                aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    @csrf
                                </form>
                            </div>
                        </li>
                    </ul>
                    @endguest
                </div>
            </div>
        </nav>
        <main class="py-4">
            <div class="spinner">
                <div class="double-bounce1"></div>
                <div class="double-bounce2"></div>
            </div>
            @yield('content')
        </main>
    </div>
</body>
    @include('layouts.scripts')

</html>