<script>
    $(document).ready(function(){
        $(".spinner").css("display", "none");

        $(document).ajaxStart(function(){
            $(".spinner").css("display", "block");
        });
        $(document).ajaxComplete(function(){
            $(".spinner").css("display", "none");
        });

    });
    function isFormValid(attr_id){
        var result = true;
        $('#'+attr_id+' .form-group').each(function(){
            if($(this).hasClass('has-error')){
                result = false;
                return false;
            }
        });
        return result;
    }
    function getFormData(attrID) {
        var inputs = $("#"+attrID+' :input');
        var data = {};
        inputs.each(function() {
            data[this.id] = $(this).val();
        });
        return data;
    }
    function getFormDataByAttr(attrID) {
        var inputs = $("#"+attrID+' :input');
        var data = {};
        inputs.each(function() {
            data[this.getAttribute('title')] = $(this).val();
        });
        return data;
    }

    var EasyAjax = new function () {
    this.post = function (data,url) {
        return $.ajax({
            data: {data:data},
            url: url,
            type: 'POST',
            beforeSend: function (request) {
                return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
            }
        });
    }
    this.get = function (url) {
        return $.ajax({
            url: url,
            type: 'GET',
            beforeSend: function (request) {
                return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
            },
        });
    }
    this.put = function (data,url) {
        return $.ajax({
            data: {data:data},
            url: url,
            type: 'PUT',
            beforeSend: function (request) {
                return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
            }
        });
    }
    this.delete = function (url) {
        return $.ajax({
            url: url,
            type: 'DELETE',
            beforeSend: function (request) {
                return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
            }
        });
    }
}

</script>