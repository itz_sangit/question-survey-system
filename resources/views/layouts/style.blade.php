<style>
    ul.cboxtags {
        list-style: none;
        padding: 20px;
    }

    ul.cboxtags li {
        display: inline;
    }

    ul.cboxtags li label {
        display: inline-block;
        background-color: rgba(255, 255, 255, .9);
        border: 2px solid rgba(139, 139, 139, .3);
        color: #adadad;
        border-radius: 25px;
        white-space: nowrap;
        margin: 3px 0px;
        -webkit-touch-callout: none;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
        -webkit-tap-highlight-color: transparent;
        transition: all .2s;
    }

    ul.cboxtags li label {
        padding: 8px 12px;
        cursor: pointer;
    }

    ul.cboxtags li label::before {
        display: inline-block;
        font-style: normal;
        font-variant: normal;
        text-rendering: auto;
        -webkit-font-smoothing: antialiased;
        font-family: "Font Awesome 5 Free";
        font-weight: 900;
        font-size: 12px;
        padding: 2px 6px 2px 2px;
        content: "\f067";
        transition: transform .3s ease-in-out;
    }

    ul.cboxtags li input[type="checkbox"]:checked+label::before {
        content: "\f00c";
        transform: rotate(-360deg);
        transition: transform .3s ease-in-out;
    }

    ul.cboxtags li input[type="checkbox"]:checked+label {
        border: 2px solid #1bdbf8;
        background-color: #12bbd4;
        color: #fff;
        transition: all .2s;
    }

    ul.cboxtags li input[type="checkbox"] {
        display: absolute;
    }

    ul.cboxtags li input[type="checkbox"] {
        position: absolute;
        opacity: 0;
    }

    ul.cboxtags li input[type="checkbox"]:focus+label {
        border: 2px solid #e9a1ff;
    }

    .spinner {
        width: 55px;
        height: 55px;

        z-index: 9999;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
        position: fixed;
        display: block;
        margin: auto;
    }

    .double-bounce1,
    .double-bounce2 {
        width: 100%;
        height: 100%;
        border-radius: 50%;
        background-color: darkred;
        opacity: 0.6;
        position: absolute;
        top: 0;
        left: 0;

        -webkit-animation: sk-bounce 2s infinite ease-in-out;
        animation: sk-bounce 2s infinite ease-in-out;
    }

    .double-bounce2 {
        background-color: #0b3e6f;
    }

    .double-bounce2 {
        -webkit-animation-delay: -1s;
        animation-delay: -1s;
    }

    @-webkit-keyframes sk-bounce {
        0%,
        100% {
            -webkit-transform: scale(0);
        }
        50% {
            -webkit-transform: scale(1);
        }
    }

    @keyframes sk-bounce {
        0%,
        100% {
            transform: scale(0);
            -webkit-transform: scale(0);
        }
        50% {
            transform: scale(1);
            -webkit-transform: scale(1);
        }
    }

    .form-group.has-error label {
        color: #dd4b39;
    }

    .form-group.has-error .form-control,
    .form-group.has-error .input-group-addon {
        border-color: #dd4b39;
        box-shadow: none;
    }

    .form-group.has-error .help-block {
        color: #dd4b39;
    }

    .inno-box {
        border: 25px solid white;
        box-sizing: border-box;
        z-index: 1;
    }

    .inno-btn {
        background: #3583A9;
        color: white;
    }

    .inno-btn-li-active {
        border-bottom: 8px solid #115d78;
        color: black;

    }

    .inno-btn-li:hover {
        border-bottom: 8px solid #6CA5C0;
        color: black;
    }

    .inno-btn-li {
        color: black;
    }
</style>