@extends('layouts.app') 
@section('content')
<div class="container">
    <div class="row justify-content-center" style="">
        <div class="col-md-10 inno-box">
            <div class="jumbotron jumbotron-fluid">
                <div class="container">
                    <h1 class="display-4">Please Fill out the Questions</h1>
                    <p class="lead">Choose an answer from each quetion.</p>
                    <hr>
                    <div id="questionBank">
                        
                    </div>
                    <hr>
                    <button class="btn btn-md btn-block inno-btn" onclick="submit()"> Submit answers</button>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
    {{-- external script for this page --}}
    @include('survey.scripts')

@endsection