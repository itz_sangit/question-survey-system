<script>
    var questions = @json($questions)

   $(function(){
    load_question();
   }) 
   function load_question() {
       questions.forEach((element,key)=>{           
            $("#questionBank").append(question_card_html(element,key));
       })
   }
   function question_card_html(data,index) {

       
       return `<li class="card" id="question${index}">
                    <div class="card-body">
                        <h3 class="h3">${index+1}. ${data.text} </h3>
                        <ul class="cboxtags">
                            ${data.options.map((element,key)=>{
                                return `<li><input type="checkbox" onclick="get_answer(${index},${key})" id="check1_${index}_${key}" value="${element}"><label for="check1_${index}_${key}">${element}</label></li>`
                            }).join('')}
                         </ul>
                    </div>
                </li>`
   }
   function get_answer(q_index,answer) {
       if (questions[q_index].choose_index  != -1) {
           //unchecking if already checked some question
          $(`#check1_${q_index}_${questions[q_index].choose_index}`).prop('checked', false); 
          if (questions[q_index].choose_index == answer) {
            questions[q_index].choose_index = -1; 
          }
          else{
            questions[q_index].choose_index = answer; 
          }
       }
       else{
         questions[q_index].choose_index = answer;
       }       
   }
   
   checkValidToSubmit = () => {
        let check = false;
        questions.forEach(element=>{            
            if(element.choose_index != -1) check = true;
        });
        return check;
   }

   function submit() {
        if (checkValidToSubmit()) {
            Swal.fire({
                title: 'Are you sure?',
                text: "Do you want to submit the survey?",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, submit!'
                }).then((result) => {
                if (result.value) {
                    EasyAjax.post(questions,"submit-survey").done(function(){
                    window.location.reload(); 
                    });
                }
           });  
        }
        else{
            Swal.fire(
                'Problem',
                'No Data is Provided',
                'error'
                );
        }
   }

</script>