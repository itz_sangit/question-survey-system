@extends('layouts.app') 
@section('content')
<div class="container">
    <div class="row inno-box" style="padding:1%">
        <a href="{{URL::to('/export')}}" class="btn btn-block inno-btn" style="margin:2%;"> Export Data to Excel</a>
        <div class="col-sm-8">
            <div class="card">
                <div class="card-body">
                    <h3>Frequency of options selected from each question</h3>
                    <div id="canvasWrapper" style="position: relative; height:500px">
                        <canvas id="chart"></canvas>
                    </div>
                        
                </div>
            </div>
        </div>
        <div class="col-sm-4 table-responsive">
            <table class="table table-hover table-light" id="question_table" style="width:100%">
                <tr>
                    <th>Question</th>
                    <th>Option</th>
                    <th>Frequency</th>
                </tr>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
</div>
<script>
    $(function(){
        $('#question_table').DataTable({
            processing: true,
            serverSide: true,
            "lengthMenu": [[5,10, 25], [5,10, 25]],         
            "bInfo" : false,
            ajax: 'submitted-answer-ssr',
            columns: [
                { "title": "Question",data: 'question', name: 'question'},
                { "title": "Option ",data: 'data', name: 'question'},
                { "title": "Frequency",data: 'count', name: 'question'},
            ]
        });
    });
     $(document).ready(function() {
        var ctx = document.getElementById('chart').getContext('2d');
        @if (isset($chart_data['label']))
            var chart_data = @json($chart_data);
        @else
            var chart_data = [];
        @endif

        var chartOptions = {
            responsive: true,
            maintainAspectRatio: false,
            legend: {
                position: "bottom",
                labels: {
                    fontColor: 'black'
                }
            },
            title: {
                display: true,
                text: "Bar chart Each Question's Options"
            },
            scales: {
                yAxes: [{
                ticks: {
                    beginAtZero: true
                }
                }]
            },
            tooltips: {
                enabled: true
            },
            animation: {
              duration: 1,
              onComplete: function () {
                var chartInstance = this.chart,
                    ctx = chartInstance.ctx;
                    ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, Chart.defaults.global.defaultFontStyle, Chart.defaults.global.defaultFontFamily);
                    ctx.textAlign = 'left';
                    ctx.textBaseline = 'bottom';

                    this.data.datasets.forEach(function (dataset, i) {
                        var meta = chartInstance.controller.getDatasetMeta(i);
                        meta.data.forEach(function (bar, index) {
                            var data = dataset.data[index];                                                    
                            ctx.fillText(data, bar._model.x, bar._model.y - 5);
                        });
                    });
               }
             }
        };

        var Bar = new Chart(ctx, {
            type: 'bar',
            data: {
                labels: chart_data['label'],
                datasets: chart_data['answer']
            },    
            options: chartOptions
        });
    });

</script>
@endsection