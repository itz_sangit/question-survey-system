<?php

namespace App\Http\Controllers;

use App\Model\Answer;
use App\Model\Question;

use Illuminate\Http\Request;

class SurveyController extends Controller
{

    public function index()
    {
        $questions =  Question::all();
        
        return view('survey.index',compact('questions'));
    }
    public function store(Request $request)
    {

        $data = $request['data'];
        foreach ($data as $key => $value) {
            if ($value['choose_index'] != -1) {
                $data =  new Answer();
                $data->question_id =  $value['id'];
                $data->question =  $value['text'];
                $data->option_json =  \json_encode($value['options']);
                $data->selected_answer =  $value['choose_index'];
                $data->save();
            }
        }
        return $request;
    }
}
