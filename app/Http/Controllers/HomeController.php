<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Answer;
use Yajra\Datatables\Datatables;
use Rap2hpoutre\FastExcel\FastExcel;
use Carbon\Carbon;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        $chart_data =  $this->generate_chart_data();
        return view('home', compact('chart_data'));
    }
    public function generate_chart_data()
    {
        $data = Answer::orderBy('question_id', 'asc')->orderBy('selected_answer', 'asc')->get();

        $answer = $data->groupBy('selected_answer')->map(function ($item) use ($data) {
            return [
                'label' => 'Answer: ' . ($item->first()->selected_answer + 1),
                'data_set' => $item->groupBy('question_id')->map(function ($item,$key) {
                    return [
                        'key'   => $item->first()->question_id,
                        'count' => sizeof($item)
                    ];
                })->values(),
                'data' =>$data->groupBy('question_id')->map(function($item){
                    return 0;
                }),
                'backgroundColor' => $this->get_color_code($item->first()->selected_answer)

            ];
        });

        //mapping 
        $answer = $answer->map(function($item){
            foreach ($item['data_set'] as $key => $value) {
                $item['data'][$value['key']] = $value['count'];
            }
            unset($item['data_set']);
            $item['data'] = $item['data']->values();
            return $item;
        })->values();

        $label = $data->groupBy('question_id')->map(function ($item) {
            return 'Question: ' . ($item->first()->question_id);
        })->values();

        return compact(['answer', 'label']);
    }
    
    public function get_color_code($key)
    {
        if ($key == 0) {
            return '#EBCCD1';
        } else if ($key == 1) {
            return '#FAEBCC';
        } else if ($key == 2) {
            return '#D6E9C6';
        } else if ($key == 3) {
            return 'FFFFFF';
        } else if ($key == 4) {
            return '#D89D40';
        } 
        else if ($key == 5) {
            return 'red';
        }
        else if ($key == 6) {
            return 'green';
        }
        else if ($key == 7) {
            return 'yellow';
        }
        else {
            return '#FFFFFF';
        }
    }

    public function ssr_answer_submitted()
    {
        $data = Answer::get()->groupBy('question_id')->map(function($item){
            return [
                'question' => $item->first()->question_id,
                'data_set' => $item->groupBy('selected_answer')->map(function ($item,$key) {
                    return [
                        'Answer'   => $item->first()->selected_answer,
                        'count' => sizeof($item)
                    ];
                })->values()
            ];
        }) ;   
        return Datatables::of($data)
        ->addColumn('question', function ($values) {
                return 'Ques. '. $values['question'];
        })
        ->addColumn('data', function ($values) {
            $table =  '';
            foreach ($values['data_set'] as $key => $value) {
                $table = $table .  '
                        Option '.($value['Answer'] + 1) .'<br>';
            }
            return $table;
        })
        ->addColumn('count', function ($values) {
            $table =  '';
            foreach ($values['data_set'] as $key => $value) {
                $table = $table 
                       .$value['count'] . '<br>';
            }
            return $table;
        })
         ->rawColumns(['data','count'])
        ->make(true);   
    }
    function export(){
        $data = Answer::orderBy('id','desc')->get()->map(function($item){
            return [
                'Participant No ' => $item->id,
                'Question No ' => $item->question_id,
                'Question ' => $item->question,
                'Answer ' => json_decode($item['option_json'])[$item->selected_answer],
                'Submitted At ' => Carbon::parse( $item->created_at)->format('D M Y H:i:s')
            ];
        });

        return (new FastExcel($data))->download('Survey Report.xlsx');

    }
}
