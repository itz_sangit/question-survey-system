<?php

namespace App\Http\Controllers;

use App\Model\Question;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;

class QuestionController extends Controller
{

    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = Question::query()->withTrashed();
            return Datatables::of($data)
                ->addColumn('action', function ($value) {
                    if ($value->trashed()) {
                        return '<button class="btn btn-success btn-sm" onclick="edit_question('.$value->id.')" data-toggle="modal" data-target="#question_modal"><i class="fas fa-pencil-alt"></i></button>' .
                            '<button class="btn btn-warning btn-sm" onclick="deleteQuestion('.$value->id.')"><i class="fas fa-trash-restore"></i></button>';
                    } else {
                        return '<button class="btn btn-success btn-sm" onclick="edit_question('.$value->id.')" data-toggle="modal" data-target="#question_modal"><i class="fas fa-pencil-alt"></i></button>' .
                            '<button class="btn btn-danger btn-sm" onclick="deleteQuestion('.$value->id.')"><i class="fas fa-trash"></i></button>';
                    }
                })
                ->make(true);
        }
        return view('question.index');
    }

    public function store(Request $request)
    {
         $data =  $request->all()['data'];
        unset($data[0]);
        $data['options_json'] =  json_encode($data['options_json']);

        $quest =  new Question($data);
        unset($quest[0]);
        unset($quest[1]);
        $quest->save();

    }
    public function show($question)
    {
        return Question::withTrashed()->find($question);
    }

    public function update(Request $request,$question)
    {
        $data =  $request->all()['data'];
        unset($data[0]);
        $data['options_json'] =  json_encode($data['options_json']);

        $quest =  Question::withTrashed()->find($question);
        $quest->fill($data);
        unset($quest[0]);
        unset($quest[1]);
        $quest->save();
    }

    public function destroy($id)
    {
        $question = Question::withTrashed()->find($id);
        if($question->trashed()){
            $question->restore();
        } 
        else{
            $question->delete();
        }
    }
}
