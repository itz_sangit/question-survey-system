<?php
// Author : sangit
namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Question extends Model
{
    use SoftDeletes;

    //mutators
    protected $attributes = ['options','choose_index'];
    protected $appends = ['options','choose_index'];
    protected $fillable = ['options_json','text'];

    public function getOptionsAttribute()
    {
        return json_decode($this->options_json);
    }
    public function getChooseIndexAttribute()
    {
        return -1;
    }
}
