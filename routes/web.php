<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::resource('/questions', 'QuestionController');

Route::post('/submit-survey', 'SurveyController@store');
Route::get('/', 'SurveyController@index');
Route::get('/submitted-answer-ssr', 'HomeController@ssr_answer_submitted');
Route::get('/export', 'HomeController@export');
